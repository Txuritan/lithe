# Lithe Docs

## Lithe Archive (`.lar`)

`.lar` is a file format based off Quake's `.pak` file format, and is used to allow for portable pre-compile versions of programs along with its required data files (normally used for video game mods).

The differences between `.lar` and `.pak` is that the header offset does not exist as the entry table is always right after the header. This brings the header size down to 8 bytes. The last is an entry, the only change here is that instead of the entry name being 56 bytes long its 120, doubling the size of allowed paths.
