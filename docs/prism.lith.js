(function(Prism) {
    Prism.languages.lith =  Prism.languages.extend('clike', {
        'class-name': {
            pattern: /(\b(?:enum|struct|class)\s+)\w+/,
            lookbehind: true
        },
        'comment': {
            pattern: /\-\-.*/,
            lookbehind: true
        },
        'builtin': /\b(self|Self)\b/,
        'keyword': /\b(_|let|const|mut|fn|return|if|else|match|loop|for|in|while|continue|break|mod|pub|use|class|enum|struct|trait|impl|extends|alias)\b/,
        'boolean': /\b(false|true)\b/,
        'number': [
            /\b(0b[01](_?[01])*)\b/,
            /\b(0o[0-7](_?[0-7])*)\b/,
            /\b(0x[\dA-Fa-f](_?[\dA-Fa-f])*)\b/,
            /\b((\d(_?\d)*\.)?(\d(_?\d)*)([eE][+-]?\d(_?\d)*)?)\b/
        ],
        "string": {
            pattern: /"(\\|(?!")[\s\S])*"/,
            greedy: true,
            inside: {
                interpolation: {
                    pattern: /\$\([^)]+\)/,
                    inside: {
                        'delimiter': {
                            pattern: /\$\(|\)/,
                            alias: 'tag'
                        },
                        rest: Prism.languages.slap
                    }
                }
            }
        },
        'char': {
            pattern: /b?'(?:\\(?:x[0-7][\da-fA-F]|u{(?:[\da-fA-F]_*){1,6}|.)|[^\\\r\n\t'])'/,
            alias: 'string'
        },
        'attribute': {
            pattern: /#!?\[.*\]/,
            greedy: true,
            alias: 'attr-name'
        },
        'punctuation': /->|\.\.=|\.{1,3}|::|[{}[\];(),:]/,
    });
}(Prism));