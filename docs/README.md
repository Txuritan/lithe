# Example

```lith
use myriad_mod::{Block, Item, Logger, Mod, Resource}
use myriad_mod::events::{Events, PreInit, RightClick, Subscribe}

#[Mod("iron", "1.0.0.0", ["Txuritan"])]
pub class Iron {
    const log = Logger::from_string("Iron")

    const iron_ore = Ore::new()
    const iron_ingot = Ingot::new()

    pub fn new() : Iron {
        Self {}
    }

    #[Subscribe(Events::PreInit)]
    pub fn pre_init(event: PreInit) {
        event.register_block(iron_ore)
        event.register_item(iron_ingot)

        event.register_smelting_recipe(iron_ore, iron_ingot, 0.7)
    }
}

#[Resource("ingot", [16, 16])]
pub class Ingot : Item {
    const log = Logger::from_string("Iron/Ingot")

    pub fn new() : Ingot {
        Self {}
    }

    #[Subscribe(Events::RightClick)]
    pub fn right_click(event: RightClick) {
        log.info("You Just Right Clicked Iron Ingot... For Some Reason...")
    }
}

#[Resource("ore", [16, 16])]
pub class Ore : Block {
    const log = Logger::from_string("Iron/Ore")

    pub fn new() : Ore {
        Self {}
    }

    #[Subscribe(Events::RightClick)]
    pub fn right_click(event: RightClick) {
        log.info("You Just Right Clicked Iron Ore... For Some Reason...")
    }
}
```
