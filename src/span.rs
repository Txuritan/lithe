#[derive(Clone, Copy, Debug, Default, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Span {
    pub start: LineColumn,
    pub end: LineColumn,
}

impl Span {
    pub(crate) fn new(start: LineColumn, end: LineColumn) -> Self {
        Self { start, end }
    }

    pub(crate) fn single(start: LineColumn) -> Self {
        Self {
            start,
            end: start.next_column(),
        }
    }

    pub(crate) fn double(start: LineColumn) -> Self {
        Self {
            start,
            end: start.next_column().next_column(),
        }
    }
}

impl std::fmt::Display for Span {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}, {})", self.start, self.end)?;

        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LineColumn {
    pub column: usize,
    pub line: usize,
}

impl LineColumn {
    pub(crate) fn new(column: usize, line: usize) -> Self {
        Self { column, line }
    }

    pub(crate) fn bump_column(&mut self) {
        self.column += 1;
    }

    pub(crate) fn bump_line(&mut self) {
        self.column = 1;
        self.line += 1;
    }

    pub(crate) fn next_column(&self) -> Self {
        Self {
            column: self.column + 1,
            line: self.line,
        }
    }
}

impl Default for LineColumn {
    fn default() -> Self {
        Self { column: 1, line: 1 }
    }
}

impl std::fmt::Display for LineColumn {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.line, self.column)?;

        Ok(())
    }
}
