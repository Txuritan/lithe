pub mod syntax;

pub mod error;
pub mod span;

pub mod archive;
pub mod vm;

pub use crate::error::Error;

const TEST: &str = r#"-- Program entry
fn main(args: Array<String>) {
    -- A non mutable variable
    let yunodoc = YUNoDoc::new(args)

    -- A variable function call
    yunodoc.run()
}"#;

use crate::syntax::{
    ast::File,
    lexer,
    stream::{Parse, Stream},
};

fn main() {
    // println!(
    //     "{:#?}",
    //     Parser::parse_str("use myriad_mod::{Block, Item, Logger, Mod, Resource}")
    // );
    println!(
        "{:?}",
        File::parse(&mut Stream::new(&lexer::lex(&TEST).unwrap()))
    );
}
