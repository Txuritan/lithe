#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Error {
    UnexpectedEOF,
    UnexpectedPunctuatedState(bool, bool),
    UnexpectedToken(String),

    Empty,
}
