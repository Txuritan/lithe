use {
    crate::{
        syntax::token::{Other, Token},
        Error,
    },
    std::iter::Iterator,
};

pub struct Stream<'t> {
    tokens: &'t [Token],
    iterator_index: usize,
    peek_index: usize,
}

impl<'t> Stream<'t> {
    pub fn attempt<T>(
        &mut self,
        run: impl FnOnce(Stream<'t>) -> Result<(Option<T>, Stream<'t>), Error>,
    ) -> Result<Option<T>, Error> {
        let stepper = Stream {
            tokens: self.tokens,
            iterator_index: self.iterator_index,
            peek_index: self.peek_index,
        };

        let (res, step): (Option<T>, Stream<'t>) = run(stepper)?;

        if res.is_some() {
            self.iterator_index = step.iterator_index;
            self.peek_index = step.iterator_index;
        }

        Ok(res)
    }

    pub fn current(&self) -> Option<&'t Token> {
        self.tokens.get(self.iterator_index)
    }

    pub fn peek(&mut self) -> Option<&'t Token> {
        self.peek_index += 1;

        self.tokens.get(self.peek_index)
    }

    pub fn reset_peek(&mut self) {
        self.peek_index = self.iterator_index;
    }
}

impl<'t> Iterator for Stream<'t> {
    type Item = &'t Token;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            self.iterator_index += 1;

            match self.tokens.get(self.iterator_index) {
                Some(Token::Other(Other::Space(_))) | Some(Token::Other(Other::Tab(_))) => {
                    continue;
                }
                Some(Token::Other(Other::NewLine(_))) => {
                    continue;
                }
                Some(Token::Other(Other::SOF(_))) | Some(Token::Other(Other::EOF(_))) | None => {
                    return None;
                }
                token => return token,
            }
        }
    }
}
