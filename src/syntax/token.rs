use crate::{span::Span, syntax::lit::Lit};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Ident {
    pub value: String,
    pub span: Span,
}

impl Ident {
    pub fn new(value: String, span: impl Into<Span>) -> Self {
        Self {
            value,
            span: span.into(),
        }
    }
}

impl From<Ident> for Token {
    fn from(inner: Ident) -> Token {
        Token::Ident(inner)
    }
}

macro_rules! define_whitespace {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    }
}

define_whitespace! {
    EOF             /// `end of file`
    NewLine         /// `\n`/`\r\n`
    SOF             /// `start of file`
    Space           /// ` `
    Tab             /// `\t`
}

macro_rules! define_keywords {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_keywords! {
    As              /// `as`
    Break           /// `break`
    Const           /// `const`
    Continue        /// `continue`
    Else            /// `else`
    Enum            /// `enum`
    For             /// `for`
    Function        /// `fn`
    If              /// `if`
    Impl            /// `impl`
    In              /// `in`
    Infix           /// `infix`
    Let             /// `let`
    Loop            /// `loop`
    Match           /// `match`
    Mod             /// `mod`
    Mut             /// `mut`
    Pub             /// `pub`
    Return          /// `return`
    SelfType        /// `Self`
    SelfValue       /// `self`
    Struct          /// `struct`
    Trait           /// `trait`
    TypeAlias       /// `typealias`
    Use             /// `use`
    VarArg          /// `vararg`
    Where           /// `where`
    While           /// `while`
}

macro_rules! define_symbols {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_symbols! {
    Add             /// `+`
    AddEq           /// `+=`
    And             /// `&`
    AndAnd          /// `&&`
    AndEq           /// `&=`
    At              /// `@`
    Bang            /// `!`
    BraceClose      /// `}`
    BraceOpen       /// `{`
    BracketClose    /// `]`
    BracketOpen     /// `[`
    Caret           /// `^`
    CaretEq         /// `^=`
    Colon           /// `:`
    ColonColon      /// `::`
    Comma           /// `,`
    Div             /// `/`
    DivEq           /// `/=`
    Dot             /// `.`
    DotDot          /// `..`
    DotDotEq        /// `..=`
    Eq              /// `=`
    EqEq            /// `==`
    Ge              /// `>=`
    Gt              /// `>`
    LArrow          /// `<-`
    Le              /// `<=`
    Lt              /// `<`
    Mul             /// `*`
    MulEq           /// `*=`
    Ne              /// `!=`
    Or              /// `|`
    OrOr            /// `||`
    ParenClose      /// `)`
    ParenOpen       /// `(`
    Pound           /// `#`
    Question        /// `?`
    RArrow          /// `->`
    Rem             /// `%`
    RemEq           /// `%=`
    Shl             /// `<<`
    ShlEq           /// `<<=`
    Shr             /// `>>`
    ShrEq           /// `>>=`
    Sub             /// `-`
    SubEq           /// `-=`
    Tilde           /// `~`
    Underscore      /// `_`
}

macro_rules! define_token {
    ( $( $name:ident #[$doc:meta] )* ) => {
        #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
        pub enum Token {
            Ident(Ident),
            Literal(Lit),
            $(
                #[$doc]
                $name($name),
            )*
        }

        impl Token {
            pub fn new(inner: impl Into<Token>) -> Token {
                inner.into()
            }
        }

        $(
            impl From<$name> for Token {
                fn from(inner: $name) -> Token {
                    Token::$name(inner)
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_token! {
    EOF             /// `end of file`
    NewLine         /// `\n`/`\r\n`
    SOF             /// `start of file`
    Space           /// ` `
    Tab             /// `\t`

    As              /// `as`
    Break           /// `break`
    Const           /// `const`
    Continue        /// `continue`
    Else            /// `else`
    Enum            /// `enum`
    For             /// `for`
    Function        /// `fn`
    If              /// `if`
    Impl            /// `impl`
    In              /// `in`
    Infix           /// `infix`
    Let             /// `let`
    Loop            /// `loop`
    Match           /// `match`
    Mod             /// `mod`
    Mut             /// `mut`
    Pub             /// `pub`
    Return          /// `return`
    SelfType        /// `Self`
    SelfValue       /// `self`
    Struct          /// `struct`
    Trait           /// `trait`
    TypeAlias       /// `typealias`
    Use             /// `use`
    VarArg          /// `vararg`
    Where           /// `where`
    While           /// `while`

    Add             /// `+`
    AddEq           /// `+=`
    And             /// `&`
    AndAnd          /// `&&`
    AndEq           /// `&=`
    At              /// `@`
    Bang            /// `!`
    BraceClose      /// `}`
    BraceOpen       /// `{`
    BracketClose    /// `]`
    BracketOpen     /// `[`
    Caret           /// `^`
    CaretEq         /// `^=`
    Colon           /// `:`
    ColonColon      /// `::`
    Comma           /// `,`
    Div             /// `/`
    DivEq           /// `/=`
    Dot             /// `.`
    DotDot          /// `..`
    DotDotEq        /// `..=`
    Eq              /// `=`
    EqEq            /// `==`
    Ge              /// `>=`
    Gt              /// `>`
    LArrow          /// `<-`
    Le              /// `<=`
    Lt              /// `<`
    Mul             /// `*`
    MulEq           /// `*=`
    Ne              /// `!=`
    Or              /// `|`
    OrOr            /// `||`
    ParenClose      /// `)`
    ParenOpen       /// `(`
    Pound           /// `#`
    Question        /// `?`
    RArrow          /// `->`
    Rem             /// `%`
    RemEq           /// `%=`
    Shl             /// `<<`
    ShlEq           /// `<<=`
    Shr             /// `>>`
    ShrEq           /// `>>=`
    Sub             /// `-`
    SubEq           /// `-=`
    Tilde           /// `~`
    Underscore      /// `_`
}

#[rustfmt::skip]
#[macro_export]
macro_rules! T {
    (as)            => { $crate::syntax::token::As };
    (break)         => { $crate::syntax::token::Break };
    (const)         => { $crate::syntax::token::Const };
    (continue)      => { $crate::syntax::token::Continue };
    (else)          => { $crate::syntax::token::Else };
    (enum)          => { $crate::syntax::token::Enum };
    (fn)            => { $crate::syntax::token::Function };
    (for)           => { $crate::syntax::token::For };
    (if)            => { $crate::syntax::token::If };
    (impl)          => { $crate::syntax::token::Impl };
    (in)            => { $crate::syntax::token::In };
    (infix)         => { $crate::syntax::token::Index };
    (let)           => { $crate::syntax::token::Let };
    (loop)          => { $crate::syntax::token::Loop };
    (match)         => { $crate::syntax::token::Match };
    (mod)           => { $crate::syntax::token::Mod };
    (mut)           => { $crate::syntax::token::Mut };
    (pub)           => { $crate::syntax::token::Pub };
    (return)        => { $crate::syntax::token::Return };
    (self)          => { $crate::syntax::token::SelfType };
    (Self)          => { $crate::syntax::token::SelfVar };
    (struct)        => { $crate::syntax::token::Struct };
    (trait)         => { $crate::syntax::token::Trait };
    (typealias)     => { $crate::syntax::token::TypeAlias };
    (use)           => { $crate::syntax::token::Use };
    (vararg)        => { $crate::syntax::token::VarArg };
    (where)         => { $crate::syntax::token::Where };
    (while)         => { $crate::syntax::token::While };

    (+)             => { $crate::syntax::token::Add };
    (+=)            => { $crate::syntax::token::AddEq };
    (&)             => { $crate::syntax::token::And };
    (&&)            => { $crate::syntax::token::AndAnd };
    (&=)            => { $crate::syntax::token::AndEq };
    (@)             => { $crate::syntax::token::At };
    (!)             => { $crate::syntax::token::Bang };
    (^)             => { $crate::syntax::token::Caret };
    (^=)            => { $crate::syntax::token::CaretEq };
    (:)             => { $crate::syntax::token::Colon };
    (::)            => { $crate::syntax::token::ColonColon };
    (,)             => { $crate::syntax::token::Comma };
    (/)             => { $crate::syntax::token::Div };
    (/=)            => { $crate::syntax::token::DivEq };
    (.)             => { $crate::syntax::token::Dot };
    (..)            => { $crate::syntax::token::DotDot };
    (..=)           => { $crate::syntax::token::DotDotEq };
    (=)             => { $crate::syntax::token::Eq };
    (==)            => { $crate::syntax::token::EqEq };
    (>=)            => { $crate::syntax::token::Ge };
    (>)             => { $crate::syntax::token::Gt };
    (<-)            => { $crate::syntax::token::LArrow };
    (<=)            => { $crate::syntax::token::Le };
    (<)             => { $crate::syntax::token::Lt };
    (*)             => { $crate::syntax::token::Mul };
    (*=)            => { $crate::syntax::token::MulEq };
    (!=)            => { $crate::syntax::token::Ne };
    (|)             => { $crate::syntax::token::Or };
    (||)            => { $crate::syntax::token::OrOr };
    (#)             => { $crate::syntax::token::Pound };
    (?)             => { $crate::syntax::token::Question };
    (->)            => { $crate::syntax::token::RArrow };
    (%)             => { $crate::syntax::token::Rem };
    (%=)            => { $crate::syntax::token::RemEq };
    (<<)            => { $crate::syntax::token::Shl };
    (<<=)           => { $crate::syntax::token::ShlEq };
    (>>)            => { $crate::syntax::token::Shr };
    (>>=)           => { $crate::syntax::token::ShrEq };
    (-)             => { $crate::syntax::token::Sub };
    (-=)            => { $crate::syntax::token::SubEq };
    (~)             => { $crate::syntax::token::Tilde };
    (_)             => { $crate::syntax::token::Underscore };
}
