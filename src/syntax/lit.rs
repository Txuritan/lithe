use crate::{span::Span, syntax::token::Token, Error};

macro_rules! define_lit {
    ( $( $name:ident $typ:ident #[$doc:meta] )* ) => {
        #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
        pub enum Lit {
            $(
                #[$doc]
                $name($typ),
            )*
        }

        impl From<Lit> for Token {
            fn from(inner: Lit) -> Self {
                Token::Literal(inner)
            }
        }

        $(
            impl From<$typ> for Token {
                fn from(inner: $typ) -> Self {
                    Token::Literal(Lit::$name(inner))
                }
            }
        )*
    };
}

define_lit! {
    Bool        LitBool         /// A literal `true`/`false`
    Byte        LitByte         /// A literal `b'a'`
    ByteStr     LitByteStr      /// A literal `b"abc"`
    Char        LitChar         /// A literal `'a'`
    Float       LitFloat        /// A literal `1.0`
    Int         LitInt          /// A literal `1`
    Str         LitStr          /// A literal `"abc"`
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitChar {
    pub token: Literal,
    pub suffix: String,
}

impl LitChar {
    pub fn new(c: char, span: impl Into<Span>) -> Self {
        LitChar {
            token: Literal {
                text: c.to_string(),
                span: span.into(),
            },
            suffix: String::new(),
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitByte {
    pub token: Literal,
    pub suffix: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitByteStr {
    pub token: Literal,
    pub suffix: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitStr {
    pub token: Literal,
    pub suffix: String,
}

impl LitStr {
    pub fn new(text: String, span: impl Into<Span>) -> Self {
        LitStr {
            token: Literal {
                text,
                span: span.into(),
            },
            suffix: String::new(),
        }
    }
}

pub struct LitNum;

impl LitNum {
    pub fn parse(buff: String, span: impl Into<Span>) -> Result<Lit, Error> {
        todo!()
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitFloat {
    pub token: Literal,
    pub digits: String,
    pub suffix: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitInt {
    pub token: Literal,
    pub digits: String,
    pub suffix: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitBool {
    pub token: Literal,
    pub value: bool,
}

impl LitBool {
    pub fn new(value: bool, span: impl Into<Span>) -> LitBool {
        LitBool {
            token: Literal {
                text: value.to_string(),
                span: span.into(),
            },
            value,
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Literal {
    pub text: String,
    pub span: Span,
}
