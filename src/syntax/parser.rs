#![allow(unused_variables)]

// use crate::{
//     syntax::{ast::File, token::Token},
//     Error,
// };

// TODO: switch over to parser based trait
// pub fn parse<'t>(stream: &'t [Token]) -> Result<File<'t>, Error> {
//     let mut ctx = Context {
//         data: &stream,
//         tokens: Cursor::new(&stream),

//         current: None,

//         items: Vec::new(),
//         span: Span::new(LineColumn::new(0, 0), LineColumn::new(0, 0)),
//     };

//     loop {
//         match ctx.next() {
//             Some(Token::Keyword(Keyword::Function(t))) => handle_function(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Mod(t))) => handle_mod(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Pub(t))) => handle_pub(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Use(t))) => handle_use(&mut ctx, t),
//             Some(Token::Keyword(Keyword::TypeAlias(t))) => handle_type_alias(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Enum(t))) => handle_enum(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Struct(t))) => handle_struct(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Trait(t))) => handle_trait(&mut ctx, t),
//             Some(Token::Keyword(Keyword::Impl(t))) => handle_impl(&mut ctx, t),
//             Some(Token::Symbol(Symbol::Pound(t))) => handle_pound(&mut ctx, t),
//             Some(Token::Other(other)) => handle_other(&mut ctx, other),
//             Some(t) => todo!("return error with unexpected token: {:?}", t),
//             None => {
//                 break;
//             }
//         }?;
//     }

//     if ctx.items.is_empty() {
//         return Err(Error::Empty);
//     }

//     Ok(File {
//         items: ctx.items,
//         span: ctx.span,
//     })

//     todo!()
// }

#[cfg(test)]
mod test {
    use crate::{
        span::{LineColumn, Span},
        syntax::{
            ast::{Block, File, Item, ItemFunction, Punctuated},
            stream::{Parse, Stream},
            token::{
                BraceClose, BraceOpen, Function, Ident, ParenClose, ParenOpen, Space, Token, EOF,
                SOF,
            },
        },
    };

    macro_rules! t {
        ($c1:expr, $l1:expr => $c2:expr, $l2:expr) => {
            Span {
                start: LineColumn {
                    column: $c1,
                    line: $l1,
                },
                end: LineColumn {
                    column: $c2,
                    line: $l2,
                },
            }
        };
        ($token:ident($param:expr); $c1:expr, $l1:expr => $c2:expr, $l2:expr ) => {
            Token::new($token::new($param, t![$c1, $l1 => $c2, $l2]))
        };
        ($token:ident; $c1:expr, $l1:expr => $c2:expr, $l2:expr ) => {
            Token::new($token::new(t![$c1, $l1 => $c2, $l2]))
        };
    }

    #[test]
    // #[should_panic]
    fn fn_main() {
        assert_eq!(
            Ok(File {
                items: vec![Item::Function(ItemFunction {
                    token: &Function::new(t![1, 1 => 3, 1]),
                    attrs: vec![],
                    ident: &Ident {
                        value: "main".into(),
                        span: t![4, 1 => 8, 1],
                    },
                    inputs: Punctuated {
                        inner: vec![],
                        last: None,
                    },
                    block: Box::new(Block { stmts: vec![] }),
                    output: None,
                })],
                // span: t![1, 1 => 13, 1]
            }),
            File::parse(&mut Stream::new(&[
                t!(SOF; 1, 1 => 1, 1),
                t!(Function; 1, 1 => 3, 1),
                t!(Space; 3, 1 => 4, 1),
                t!(Ident(String::from("main")); 4, 1 => 8, 1),
                t!(ParenOpen; 8, 1 => 9, 1),
                t!(ParenClose; 9, 1 => 10, 1),
                t!(Space; 10, 1 => 11, 1),
                t!(BraceOpen; 11, 1 => 12, 1),
                t!(BraceClose; 12, 1 => 13, 1),
                t!(EOF; 13, 1 => 13, 1),
            ])),
        );
    }
}
