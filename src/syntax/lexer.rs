use {
    crate::{
        span::{LineColumn, Span},
        syntax::{lit::*, token::*},
        Error,
    },
    std::{iter::Peekable, str::Chars},
};

macro_rules! token {
    ($ctx:ident, $t:ident, 0) => {
        $ctx.tokens
            .push(Token::new($t::new(Span::new($ctx.file_lc, $ctx.file_lc))))
    };
    ($ctx:ident, $t:ident, 1) => {
        $ctx.tokens
            .push(Token::new($t::new(Span::single($ctx.file_lc))))
    };
    ($ctx:ident, $t:ident, 2) => {
        $ctx.tokens
            .push(Token::new($t::new(Span::double($ctx.file_lc))))
    };
    ($ctx:ident, $t:ident, $s:expr) => {
        $ctx.tokens.push(Token::new($t::new($s)))
    };
    ($ctx:ident, $t:ident, $s:expr, $e:expr) => {
        $ctx.tokens.push(Token::new($t::new(Span::new($s, $e))))
    };
    ($ctx:ident, $t:ident($param:expr), $s:expr, $e:expr) => {
        $ctx.tokens
            .push(Token::new($t::new($param, Span::new($s, $e))))
    };
}

macro_rules! or {
    ($ctx:ident, $single:ident, [ $( $c:expr => $double:ident, )* ] ) => {
        or!($ctx, $single, [
            $( $c => $double ),+
        ])
    };
    ($ctx:ident, $single:ident, [ $( $c:expr => $double:ident ),* ] ) => {
        match $ctx.chars.peek().copied() {
            $(
                Some($c) => {
                    token!($ctx, $double, 2);

                    $ctx.increment = 1;
                }
            )*
            _ => token!($ctx, $single, 1),
        }
    };
}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
enum State {
    Normal,
    Ident,
    String(char, bool),
    Number,
    Comment,
}

#[derive(Clone, Debug)]
struct Context<'c> {
    tokens: Vec<Token>,
    chars: Peekable<Chars<'c>>,

    file_lc: LineColumn,
    start_lc: LineColumn,

    state: State,
    buff: String,

    increment: usize,
}

impl<'c> Context<'c> {
    fn new(src: &'c str) -> Self {
        Self {
            tokens: Vec::with_capacity(src.len()),
            chars: src.chars().peekable(),

            file_lc: LineColumn::default(),
            start_lc: LineColumn::default(),

            state: State::Normal,

            buff: String::with_capacity(20),

            increment: 0,
        }
    }
}

pub fn lex(s: &str) -> Result<Vec<Token>, Error> {
    let mut ctx = Context::new(s);

    token!(ctx, SOF, ctx.file_lc, ctx.file_lc);

    loop {
        if let Some(c) = ctx.chars.next() {
            let state = ctx.state;

            handle(&mut ctx, c)?;

            println!("{}, {:?} => {:?}", c, state, ctx.state);

            for _ in 0..ctx.increment {
                ctx.file_lc.bump_column();
                ctx.chars.next();
            }

            match ctx.tokens.last() {
                Some(Token::NewLine(NewLine { .. })) => {}
                Some(_) | None => {
                    ctx.file_lc.bump_column();
                }
            }
        } else {
            token!(ctx, EOF, ctx.file_lc, ctx.file_lc);

            break;
        }
    }

    Ok(ctx.tokens)
}

fn handle(ctx: &mut Context, c: char) -> Result<(), Error> {
    match ctx.state {
        State::Normal => handle_normal(ctx, c),
        State::Ident => handle_ident(ctx, c)?,
        State::String(delimiter, escaped) => match c {
            '\\' if !escaped => {
                ctx.state = State::String(delimiter, true);
            }
            'n' if escaped => {
                ctx.buff.push('\n');

                ctx.state = State::String(delimiter, false);
            }
            c if c == delimiter && !escaped => {
                match delimiter {
                    '"' => {
                        token!(
                            ctx,
                            LitStr(ctx.buff.clone()),
                            ctx.start_lc,
                            ctx.file_lc.next_column()
                        );

                        ctx.buff.clear();
                    }
                    '\'' => {
                        if ctx.buff.len() == 1 {
                            let ch = ctx.buff.char_indices().next().unwrap().1;

                            token!(ctx, LitChar(ch), ctx.start_lc, ctx.file_lc.next_column());

                            ctx.buff.clear();
                        } else {
                            todo!("Character literal is too long, return error");
                        }
                    }
                    _ => unreachable!(),
                }

                ctx.state = State::Normal;
            }
            c => {
                ctx.buff.push(c);

                ctx.state = State::String(delimiter, false);
            }
        },
        State::Number => match c {
            '0'..='9' | '.' | 'a'..='z' => ctx.buff.push(c),
            '_' => { /* ignore */ }
            c if c.is_whitespace() => {
                ctx.state = State::Normal;

                ctx.tokens.push(Token::new(LitNum::parse(
                    ctx.buff.clone(),
                    Span::new(ctx.start_lc, ctx.file_lc),
                )?));

                ctx.buff.clear();
            }
            _ => {}
        },
        State::Comment => {
            if let '\n' = c {
                ctx.state = State::Normal;
            }
        }
    }

    Ok(())
}

fn handle_normal(ctx: &mut Context, c: char) {
    match c {
        // Normal
        ' ' => token!(ctx, Space, 1),
        '\t' => token!(ctx, Tab, 1),
        '\n' => {
            ctx.file_lc.bump_line();

            token!(ctx, NewLine, 0);
        }
        '\r' => {}

        // Operations
        '=' => or!(ctx, Eq, [
            '=' => EqEq,
            '>' => Gt,
        ]),
        '+' => or!(ctx, Add, [ '=' => AddEq ]),
        '-' => {
            if ctx.chars.peek().copied() == Some('-') {
                ctx.state = State::Comment;
            } else {
                or!(ctx, Sub, [
                    '=' => SubEq,
                    '>' => RArrow,
                ])
            }
        }
        '/' => or!(ctx, Div, [ '=' => DivEq ]),
        '*' => or!(ctx, Mul, [ '=' => MulEq ]),
        '!' => or!(ctx, Bang, [ '=' => Ne ]),
        '%' => or!(ctx, Rem, [ '=' => RemEq ]),

        // Symbols
        '[' => token!(ctx, BracketOpen, 1),
        ']' => token!(ctx, BracketClose, 1),

        '{' => token!(ctx, BraceOpen, 1),
        '}' => token!(ctx, BraceClose, 1),

        '(' => token!(ctx, ParenOpen, 1),
        ')' => token!(ctx, ParenClose, 1),

        // TODO: `..` and `..=`
        '.' => token!(ctx, Dot, 1),
        ',' => token!(ctx, Comma, 1),
        '&' => or!(ctx, And, [ '&' => AndAnd ]),
        '|' => or!(ctx, Or, [ '|' => OrOr ]),

        '#' => token!(ctx, Pound, 1),
        '?' => token!(ctx, Question, 1),
        ':' => or!(ctx, Colon, [ ':' => ColonColon ]),
        '_' => token!(ctx, Underscore, 1),
        '~' => token!(ctx, Tilde, 1),

        '<' => or!(ctx, Lt, [ '=' => Le ]),
        '>' => or!(ctx, Gt, [ '=' => Ge ]),

        '"' | '\'' => {
            ctx.state = State::String(c, false);

            ctx.start_lc = ctx.file_lc;

            ctx.buff.clear();
        }

        '0'..='9' => {
            ctx.state = State::Number;

            ctx.buff.clear();

            ctx.buff.push(c);
        }

        c => {
            if c.is_alphanumeric() || c == '_' {
                ctx.state = State::Ident;

                ctx.start_lc = ctx.file_lc;

                ctx.buff.clear();

                ctx.buff.push(c);
            } else {
                todo!("Return error here")
            }
        }
    }
}

fn handle_ident(ctx: &mut Context, c: char) -> Result<(), Error> {
    println!("{} {}", c, c.is_alphanumeric());

    if c.is_alphanumeric() || c == '_' {
        ctx.buff.push(c);
    } else {
        ctx.state = State::Normal;

        match ctx.buff.as_str() {
            "true" => token!(ctx, LitBool(true), ctx.start_lc, ctx.file_lc),
            "false" => token!(ctx, LitBool(false), ctx.start_lc, ctx.file_lc),

            "let" => token!(ctx, Let, ctx.start_lc, ctx.file_lc),
            "const" => token!(ctx, Const, ctx.start_lc, ctx.file_lc),
            "mut" => token!(ctx, Mut, ctx.start_lc, ctx.file_lc),

            "fn" => token!(ctx, Function, ctx.start_lc, ctx.file_lc),
            "return" => token!(ctx, Return, ctx.start_lc, ctx.file_lc),

            "if" => token!(ctx, If, ctx.start_lc, ctx.file_lc),
            "else" => token!(ctx, Else, ctx.start_lc, ctx.file_lc),
            "match" => token!(ctx, Match, ctx.start_lc, ctx.file_lc),

            "loop" => token!(ctx, Loop, ctx.start_lc, ctx.file_lc),
            "for" => token!(ctx, For, ctx.start_lc, ctx.file_lc),
            "in" => token!(ctx, In, ctx.start_lc, ctx.file_lc),
            "while" => token!(ctx, While, ctx.start_lc, ctx.file_lc),
            "continue" => token!(ctx, Continue, ctx.start_lc, ctx.file_lc),
            "break" => token!(ctx, Break, ctx.start_lc, ctx.file_lc),

            "mod" => token!(ctx, Mod, ctx.start_lc, ctx.file_lc),
            "pub" => token!(ctx, Pub, ctx.start_lc, ctx.file_lc),
            "use" => token!(ctx, Use, ctx.start_lc, ctx.file_lc),

            "typealias" => token!(ctx, TypeAlias, ctx.start_lc, ctx.file_lc),

            "enum" => token!(ctx, Enum, ctx.start_lc, ctx.file_lc),
            "struct" => token!(ctx, Struct, ctx.start_lc, ctx.file_lc),
            "trait" => token!(ctx, Trait, ctx.start_lc, ctx.file_lc),
            "impl" => token!(ctx, Impl, ctx.start_lc, ctx.file_lc),
            // "class" => token!(ctx, Class, ctx.start_lc, ctx.file_lc),
            // "override" => token!(ctx, Override, ctx.start_lc, ctx.file_lc),
            "self" => token!(ctx, SelfValue, ctx.start_lc, ctx.file_lc),
            "Self" => token!(ctx, SelfType, ctx.start_lc, ctx.file_lc),

            "where" => token!(ctx, Where, ctx.start_lc, ctx.file_lc),

            // "with" => token!(ctx, With, ctx.start_lc, ctx.file_lc),
            "infix" => token!(ctx, Infix, ctx.start_lc, ctx.file_lc),
            "vararg" => token!(ctx, VarArg, ctx.start_lc, ctx.file_lc),

            _ => token!(ctx, Ident(ctx.buff.clone()), ctx.start_lc, ctx.file_lc),
        }

        ctx.buff.clear();

        handle(ctx, c)?;
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use crate::{
        span::{LineColumn, Span},
        syntax::{lexer::lex, lit::*, token::*},
    };

    macro_rules! t {
        ($c1:expr, $l1:expr => $c2:expr, $l2:expr) => {
            Span {
                start: LineColumn {
                    column: $c1,
                    line: $l1,
                },
                end: LineColumn {
                    column: $c2,
                    line: $l2,
                },
            }
        };
        ($token:ident($param:expr); $c1:expr, $l1:expr => $c2:expr, $l2:expr ) => {
            Token::new($token::new($param, t![$c1, $l1 => $c2, $l2]))
        };
        ($token:ident; $c1:expr, $l1:expr => $c2:expr, $l2:expr ) => {
            Token::new($token::new(t![$c1, $l1 => $c2, $l2]))
        };
    }

    #[test]
    fn let_assignment() {
        pretty_assertions::assert_eq!(
            Ok(vec![
                t!(SOF; 1, 1 => 1, 1),
                t!(Let; 1, 1 => 4, 1),
                t!(Space; 4, 1 => 5, 1),
                t!(Ident(String::from("ex")); 5, 1 => 7, 1),
                t!(Space; 7, 1 => 8, 1),
                t!(Eq; 8, 1 => 9, 1),
                t!(Space; 9, 1 => 10, 1),
                t!(Ident(String::from("Example")); 10, 1 => 17, 1),
                t!(ColonColon; 17, 1 => 19, 1),
                t!(Ident(String::from("new")); 19, 1 => 22, 1),
                t!(ParenOpen; 23, 1 => 24, 1),
                t!(ParenClose; 24, 1 => 25, 1),
                t!(EOF; 25, 1 => 25, 1),
            ]),
            lex("let ex = Example::new()"),
        );
    }

    #[test]
    fn fn_main() {
        pretty_assertions::assert_eq!(
            Ok(vec![
                t!(SOF; 1, 1 => 1, 1),
                t!(Function; 1, 1 => 3, 1),
                t!(Space; 3, 1 => 4, 1),
                t!(Ident(String::from("main")); 4, 1 => 8, 1),
                t!(ParenOpen; 8, 1 => 9, 1),
                t!(ParenClose; 9, 1 => 10, 1),
                t!(Space; 10, 1 => 11, 1),
                t!(BraceOpen; 11, 1 => 12, 1),
                t!(BraceClose; 12, 1 => 13, 1),
                t!(EOF; 13, 1 => 13, 1),
            ]),
            lex("fn main() {}"),
        );
    }

    #[test]
    fn fn_main_args() {
        pretty_assertions::assert_eq!(
            Ok(vec![
                t!(SOF; 1, 1 => 1, 1),
                t!(Function; 1, 1 => 3, 1),
                t!(Space; 3, 1 => 4, 1),
                t!(Ident(String::from("main")); 4, 1 => 8, 1),
                t!(ParenOpen; 8, 1 => 9, 1),
                t!(Ident(String::from("args")); 9, 1 => 13, 1),
                t!(Colon; 13, 1 => 14, 1),
                t!(Space; 14, 1 => 15, 1),
                t!(Ident(String::from("Array")); 15, 1 => 20, 1),
                t!(Lt; 20, 1 => 21, 1),
                t!(Ident(String::from("String")); 21, 1 => 27, 1),
                t!(Gt; 27, 1 => 28, 1),
                t!(ParenClose; 28, 1 => 29, 1),
                t!(Space; 29, 1 => 30, 1),
                t!(BraceOpen; 30, 1 => 31, 1),
                t!(BraceClose; 31, 1 => 32, 1),
                t!(EOF; 32, 1 => 32, 1),
            ]),
            lex("fn main(args: Array<String>) {}"),
        );
    }

    #[test]
    fn fn_main_args_print() {
        pretty_assertions::assert_eq!(
            Ok(vec![
                t!(SOF; 1, 1 => 1, 1),
                t!(Function; 1, 1 => 3, 1),
                t!(Space; 3, 1 => 4, 1),
                t!(Ident(String::from("main")); 4, 1 => 8, 1),
                t!(ParenOpen; 8, 1 => 9, 1),
                t!(Ident(String::from("args")); 9, 1 => 13, 1),
                t!(Colon; 13, 1 => 14, 1),
                t!(Space; 14, 1 => 15, 1),
                t!(Ident(String::from("Array")); 15, 1 => 20, 1),
                t!(Lt; 20, 1 => 21, 1),
                t!(Ident(String::from("String")); 21, 1 => 27, 1),
                t!(Gt; 27, 1 => 28, 1),
                t!(ParenClose; 28, 1 => 29, 1),
                t!(Space; 29, 1 => 30, 1),
                t!(BraceOpen; 30, 1 => 31, 1),
                t!(NewLine; 1, 2 => 1, 2),
                t!(Space; 1, 2 => 2, 2),
                t!(Space; 2, 2 => 3, 2),
                t!(Space; 3, 2 => 4, 2),
                t!(Space; 4, 2 => 5, 2),
                t!(Ident(String::from("print")); 5, 2 => 10, 2),
                t!(ParenOpen; 10, 2 => 11, 2),
                t!(LitStr(String::from("{:?}")); 11, 2 => 17, 2),
                t!(Comma; 17, 2 => 18, 2),
                t!(Space; 18, 2 => 19, 2),
                t!(Ident(String::from("args")); 19, 2 => 23, 2),
                t!(ParenClose; 23, 2 => 24, 2),
                t!(NewLine; 1, 3 => 1, 3),
                t!(BraceClose; 1, 3 => 2, 3),
                t!(EOF; 2, 3 => 2, 3),
            ]),
            lex(r#"fn main(args: Array<String>) {
    print("{:?}", args)
}"#),
        );
    }
}
