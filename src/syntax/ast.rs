pub mod expr;
pub mod ident;
pub mod item;
pub mod lit;
pub mod op;
pub mod pat;
pub mod path;
pub mod punctuated;
pub mod stmt;
pub mod typ;

pub use crate::syntax::ast::{
    expr::*, ident::*, item::*, lit::*, op::*, pat::*, path::*, punctuated::*, stmt::*, typ::*,
};

use crate::{
    syntax::{stream, token::Token},
    Error,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct File<'t> {
    pub items: Vec<Item<'t>>,
    // pub span: Span,
}

impl<'t> stream::Parse<'t> for File<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let mut items = Vec::new();

        match ctx.next() {
            Some(Token::Function(_)) => items.push(Item::Function(ItemFunction::parse(ctx)?)),
            Some(Token::Mod(_)) => {}
            Some(Token::Pub(_)) => {}
            Some(Token::Use(_)) => {}
            Some(Token::TypeAlias(_)) => {}
            Some(Token::Enum(_)) => {}
            Some(Token::Struct(_)) => {}
            Some(Token::Trait(_)) => {}
            Some(Token::Impl(_)) => {}
            Some(_) => {}
            None => {}
        }

        let file = File {
            items,
            // span: Span::new(LineColumn::new(0, 0), ctx.get_span().end),
        };

        Ok(file)
    }
}
