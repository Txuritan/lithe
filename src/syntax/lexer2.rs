use {
    crate::{
        span::{LineColumn, Span},
        syntax::token::Token,
    },
    std::{
        iter::{Iterator, Peekable},
        str::Chars,
    },
};

pub struct Lexer<'c> {
    tokens: Vec<Token>,

    iter: Peekable<Chars<'c>>,

    file: LineColumn,
    start: LineColumn,

    buffer: String,
}

impl<'c> Iterator for Lexer<'c> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}
