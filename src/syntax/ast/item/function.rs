use crate::{
    assert_token,
    syntax::{
        ast::{Block, PatType, Punctuated},
        stream,
        token::{self, Function, Ident, Mut, SelfValue, Token},
    },
    Error,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemFunction<'t> {
    // TODO: change to attribute type
    pub attrs: Vec<String>,
    pub token: &'t Function,
    pub ident: &'t Ident,
    pub inputs: Punctuated<FunctionArgument<'t>, Comma<'t>>,
    pub block: Box<Block<'t>>,
    // TODO: change to return type
    pub output: Option<String>,
}

impl<'t> stream::Parse<'t> for ItemFunction<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        // `fn` token
        let token = match ctx.current() {
            Some(Token::Function(token)) => token,
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        // Function name
        let ident = match ctx.next_skip() {
            Some(Token::Ident(ident)) => ident,
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        // ParenOpen
        assert_token!(ctx, Token::ParenOpen(_));

        let inputs = Punctuated::parse(ctx)?;

        // ParenClose
        assert_token!(ctx, Token::ParenClose(_));

        // Check the next token
        match ctx.peek_skip() {
            Some(Token::Colon(_)) => todo!("Handle return type"),
            Some(_) => {}
            None => {
                return Err(Error::UnexpectedEOF);
            }
        }

        // BraceOpen
        assert_token!(ctx, Token::BraceOpen(_));

        let block = Block::parse(ctx)?;

        // BraceClose
        assert_token!(ctx, Token::BraceClose(_));

        Ok(Self {
            attrs: Vec::new(),
            token,
            ident,
            inputs,
            block: Box::new(block),
            output: None,
        })
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum FunctionArgument<'t> {
    Receiver(Receiver<'t>),
    Typed(PatType<'t>),
}

impl<'t> stream::Parse<'t> for FunctionArgument<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        match ctx.step(|mut ctx| {
            let ast = Receiver::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => return Ok(FunctionArgument::Receiver(ast)),
            Err(Error::UnexpectedToken(_)) => {}
            Err(err) => return Err(err),
        }

        match ctx.step(|mut ctx| {
            let ast = PatType::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => Ok(FunctionArgument::Typed(ast)),
            Err(err) => Err(err),
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Receiver<'t> {
    // TODO: change to attribute type
    pub attrs: Vec<String>,
    pub token: &'t SelfValue,
    pub mutability: Option<&'t Mut>,
}

impl<'t> stream::Parse<'t> for Receiver<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let (mutability, token) = match ctx.next_skip() {
            Some(Token::Mut(keyword_mut)) => match ctx.next_skip() {
                Some(Token::SelfValue(token)) => (Some(keyword_mut), token),
                Some(token) => {
                    return Err(Error::UnexpectedToken(format!("{:?}", token)));
                }
                None => {
                    return Err(Error::UnexpectedEOF);
                }
            },
            Some(Token::SelfValue(token)) => (None, token),
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        Ok(Self {
            attrs: Vec::new(),
            token,
            mutability,
        })
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Comma<'t> {
    inner: &'t token::Comma,
}

impl<'t> stream::Parse<'t> for Comma<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        match ctx.next_skip() {
            Some(Token::Comma(comma)) => Ok(Self { inner: comma }),
            Some(token) => Err(Error::UnexpectedToken(format!("{:?}", token))),
            None => Err(Error::UnexpectedEOF),
        }
    }
}
