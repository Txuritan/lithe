use crate::span::Span;

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Lit {
    Str(LitStr),
    ByteStr(LitByteStr),
    Char(LitChar),
    ByteChar(LitByteChar),
    Int(LitInt),
    Float(LitFloat),
    Bool(LitBool),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitStr {
    pub value: String,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitByteStr {
    pub value: Vec<u8>,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitChar {
    pub value: char,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitByteChar {
    pub value: Vec<u8>,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitInt {
    pub digits: String,
    pub suffix: String,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitFloat {
    pub digits: String,
    pub suffix: String,
    pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct LitBool {
    pub value: bool,
    pub span: Span,
}
