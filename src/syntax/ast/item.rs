pub mod constant;
pub mod enumeration;
pub mod function;
pub mod implementation;
pub mod module;

use crate::{syntax::stream, Error};

pub use self::{
    constant::ItemConst, enumeration::ItemEnum, function::ItemFunction, implementation::ItemImpl,
    module::ItemMod,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Item<'t> {
    Const(ItemConst),
    Enum(ItemEnum),
    Function(ItemFunction<'t>),
    Impl(ItemImpl),
    Mod(ItemMod),
    Pub(ItemPub),
    Struct(ItemStruct),
    Trait(ItemTrait),
    TraitAlias(ItemTraitAlias),
    TypeAlias(ItemTypeAlias),
    Use(ItemUse),
}

impl<'t> stream::Parse<'t> for Item<'t> {
    fn parse(_ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        Err(Error::UnexpectedToken(String::from(
            "THIS IS JUST TO GET BLOCK PARSING WORKING, THIS SHOULD NOT SHOW UP",
        )))
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemPub {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemStruct {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemTrait {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemTraitAlias {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemTypeAlias {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ItemUse {}
