use crate::{
    syntax::{ast::Path, stream},
    Error,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Type<'t> {
    ImplTrait(),
    Paren(),
    Path(TypePath<'t>),
    TraitObject(),
    Tuple(),
}

impl<'t> stream::Parse<'t> for Type<'t> {
    fn parse(_ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        todo!()
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct TypePath<'t> {
    pub path: Path<'t>,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum ReturnType<'t> {
    Default,
    Type(Box<Type<'t>>),
}
