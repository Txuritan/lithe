use {
    crate::{
        syntax::{ast::op::BinOp, stream},
        Error,
    },
    std::marker::PhantomData,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Expr<'t> {
    Array(),
    Assign(ExprAssign<'t>),
    AssignOp(ExprAssignOp<'t>),
    Binary(ExprBinary<'t>),
    Block(),
    Break(),
    Call(),
    Cast(),
    Closure(),
    Continue(),
    Field(),
    For(),
    If(),
    Index(),
    Let(),
    Lit(),
    Loop(),
    Match(),
    FunctionCall(),
    Paren(),
    Range(),
    Return(),
    Struct(),
    Try(),
    Tuple(),
    Type(),
    Unary(),
    While(),
}

impl<'t> stream::Parse<'t> for Expr<'t> {
    fn parse(_ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        Err(Error::UnexpectedToken(String::from(
            "THIS IS JUST TO GET BLOCK PARSING WORKING, THIS SHOULD NOT SHOW UP",
        )))
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprAssign<'t> {
    pub left: Box<Expr<'t>>,
    pub right: Box<Expr<'t>>,
    _marker: &'t PhantomData<bool>,
    // pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprAssignOp<'t> {
    pub left: Box<Expr<'t>>,
    pub op: BinOp,
    pub right: Box<Expr<'t>>,
    _marker: &'t PhantomData<bool>,
    // pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprBinary<'t> {
    pub left: Box<Expr<'t>>,
    pub op: BinOp,
    pub right: Box<Expr<'t>>,
    _marker: &'t PhantomData<bool>,
    // pub span: Span,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct ExprBlock {
    // TODO
// pub span: Span,
}
