use crate::{
    syntax::{
        ast::{expr::Expr, item::Item, pat::Pat},
        stream,
        token::{Add, Let, Token},
    },
    Error,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Block<'t> {
    pub stmts: Vec<Stmt<'t>>,
}

impl<'t> stream::Parse<'t> for Block<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let mut stmts = Vec::new();

        loop {
            match ctx.step(|mut stream| {
                let stmt = Stmt::parse(&mut stream)?;

                Ok((stmt, stream))
            }) {
                Ok(stmt) => stmts.push(stmt),
                Err(Error::UnexpectedToken(_)) => break,
                Err(err) => return Err(err),
            }
        }

        Ok(Block { stmts })
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Stmt<'t> {
    Local(Local<'t>),
    Item(Item<'t>),
    Expr(Expr<'t>),
}

impl<'t> stream::Parse<'t> for Stmt<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        match ctx.step(|mut ctx| {
            let ast = Local::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => return Ok(Stmt::Local(ast)),
            Err(Error::UnexpectedToken(_)) => {}
            Err(err) => return Err(err),
        }

        match ctx.step(|mut ctx| {
            let ast = Item::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => return Ok(Stmt::Item(ast)),
            Err(Error::UnexpectedToken(_)) => {}
            Err(err) => return Err(err),
        }

        match ctx.step(|mut ctx| {
            let ast = Expr::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => Ok(Stmt::Expr(ast)),
            Err(err) => Err(err),
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Local<'t> {
    // TODO: change to attribute type
    pub attrs: Vec<String>,
    pub let_token: &'t Let,
    pub pat: Pat<'t>,
    pub init: Option<(&'t Add, Box<Expr<'t>>)>,
}

impl<'t> stream::Parse<'t> for Local<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        // `let` token
        let let_token = match ctx.current() {
            Some(Token::Let(token)) => token,
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        // Variable pattern
        let pat = Pat::parse(ctx)?;

        let init = match ctx.peek_skip() {
            Some(Token::Add(_)) => {
                if let Some(Token::Add(eq)) = ctx.next_skip() {
                    let expr = Expr::parse(ctx)?;

                    Some((eq, Box::new(expr)))
                } else {
                    unreachable!("Invalid state in variable parsing")
                }
            }
            _ => None,
        };

        Ok(Self {
            attrs: Vec::new(),
            let_token,
            pat,
            init,
        })
    }
}
