use {
    crate::span::LineColumn,
    std::{
        iter::Peekable,
        str::{Chars, Lines},
    },
};

pub struct Scanner<'s> {
    line_iter: Peekable<Lines<'s>>,
    line_cursor: usize,

    char_iter: Option<Chars<'s>>,
    char_queue: Vec<Option<char>>,
    char_cursor: usize,
}

impl<'s> Scanner<'s> {
    pub fn new(s: &'s str) -> Self {
        let mut lines = s.lines().peekable();
        let chars = lines.by_ref().next().map(|l| l.chars());

        Self {
            line_iter: lines,
            line_cursor: 1,

            char_iter: chars,
            char_queue: Vec::new(),
            char_cursor: 0,
        }
    }

    #[inline]
    pub fn get_pos(&self) -> LineColumn {
        LineColumn {
            line: self.line_cursor,
            column: self.char_cursor + 1,
        }
    }

    // lines
    #[inline]
    pub fn has_next_line(&mut self) -> bool {
        self.line_iter.peek().is_some()
    }

    #[inline]
    pub fn next_line(&mut self) -> &mut Self {
        if self.line_cursor < std::usize::MAX {
            self.line_cursor += 1;
        }

        self.char_queue.clear();

        self.char_iter = self.line_iter.by_ref().next().map(|l| l.chars());

        self
    }

    // chars
    #[inline]
    pub fn peek_char(&mut self) -> Option<char> {
        let stored_elements = self.char_queue.len();
        let required_elements = self.char_cursor;

        if stored_elements <= required_elements {
            for _ in stored_elements..=required_elements {
                if let Some(iter) = self.char_iter.as_mut() {
                    self.char_queue.push(iter.next());
                }
            }
        }

        self.char_queue
            .get(self.char_cursor)
            .and_then(|v| v.as_ref())
            .copied()
    }

    #[inline]
    pub fn next_char(&mut self) -> &mut Self {
        if self.char_cursor < std::usize::MAX {
            self.char_cursor += 1;
        }

        self
    }

    #[inline]
    pub fn peek_next_char(&mut self) -> Option<char> {
        if self.char_cursor < std::usize::MAX {
            self.char_cursor += 1;
        }

        let stored_elements = self.char_queue.len();
        let required_elements = self.char_cursor - 1;

        if stored_elements <= required_elements {
            for _ in stored_elements..=required_elements {
                if let Some(iter) = self.char_iter.as_mut() {
                    self.char_queue.push(iter.next());
                }
            }
        }

        self.char_queue
            .get(self.char_cursor - 1)
            .and_then(|v| v.as_ref())
            .copied()
    }

    #[inline]
    pub fn reset_char_cursor(&mut self) {
        self.char_cursor = 0;
    }
}
