use crate::span::Span;

macro_rules! define_token {
    ($name:ident, [ $( $token:ident, )* ]) => {
        define_token!($name, [ $( $token => [  ], )* ]);
    };
    ($name:ident, [ $( $token:ident => [ $( $val:ident: $typ:ty )* ], )* ]) => {
        #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
        pub enum $name {
            $(
                $token($token),
            )*
        }

        impl From<$name> for Token {
            fn from(t: $name) -> Token {
                Token::$name(t)
            }
        }

        $(
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $token {
                $( pub $val: $typ, )*
                pub span: Span,
            }

            impl $token {
                pub fn new($( $val: $typ, )* span: impl Into<Span>) -> Self {
                    Self {
                        $( $val, )*
                        span: span.into(),
                    }
                }
            }

            impl From<$token> for Token {
                fn from(t: $token) -> Token {
                    Token::$name($name::$token(t))
                }
            }

            impl std::fmt::Display for $token {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($token), self.span)?;

                    $(
                        write!(f, " {:?}", self.$val)?;
                    )*

                    Ok(())
                }
            }
        )*
    };
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Token {
    Keyword(Keyword),
    Operation(Operation),
    Symbol(Symbol),
    Other(Other),
}

impl Token {
    pub(crate) fn new(kind: impl Into<Self>) -> Self {
        kind.into()
    }

    pub(crate) fn is_whitespace(&self) -> bool {
        match self {
            Token::Other(other) => match other {
                Other::Space(_) | Other::Tab(_) | Other::NewLine(_) => true,
                _ => false,
            },
            _ => false,
        }
    }
}

#[rustfmt::skip]
define_token!(Other, [
    Ident => [ value: String ],

    Text => [ value: String ],
    Char => [ value: char ],
    Number => [ value: String ],

    Space => [],
    Tab => [],
    NewLine => [],

    SOF => [],
    EOF => [],
]);

#[rustfmt::skip]
define_token!(Keyword, [
    True, False,

    Let, Const, Mut,

    Function, Return,
    If, Else, Match,

    Loop, For, In, While,
    Continue, Break,

    Mod, Pub, Use,

    TypeAlias,

    Enum, Struct,
    Trait, Impl,
    SelfVar, SelfType,
    Class, Override,

    Where, With,
    Infix, VarArg,
]);

#[rustfmt::skip]
define_token!(Operation, [
    Assign,

    PlusAssign, MinusAssign,
    DivideAssign, MultiAssign,

    Plus, Minus,
    Divide, Multi,

    Bang, Modulo,
]);

#[rustfmt::skip]
define_token!(Symbol, [
    BracketOpen, BracketClose,
    BraceOpen, BraceClose,
    ParenOpen, ParenClose,

    Period, Comma,
    Ampersand, Pipe,
    Question, Pound,

    And, Or,

    Colon, DoubleColon,
    Arrow, FatArrow,
    Underscore, Tilde,

    LessThan, GreaterThan,
    EqualEqual, BangEqual,
    LessThanEqual, GreaterThanEqual,
]);
