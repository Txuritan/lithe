//! Lithe Archive is a custom binary archive file format made for storing compiled code and assets for plugin/mod based programs.
//!
//! There are two versions of `.lar` archives which can be figured out through their magic bytes.
//! `LA10` is the first version of the archive based of Quake's `.pak` archive, and was made to be simple to read and write.
//! `LA11` is the second version which added some new features like: variable length file paths/names and custom bit fields/flags.
//!
//! # Format
//!
//! ## Version 1.0
//!
//! Since version `1.0` was based off Quake's `.pak` format, it only changes and removed a few aspects.
//! There is no entry table offset as the list is now right after the header (always 8 bytes from the beginning).
//! And the 56 byte limit for the file name was upped to 120 bytes to allow for more complex file structures.
//!
//! ```lua
//! -- Header
//! Version [u32 | 4 bytes] (always 'LA10')
//! Entry List Size [u32 | 4 bytes]
//!
//! -- Entry Header List [128 bytes] (repeats {Entry List Size / 128} times)
//!   -- For Each Entry
//!     Entry Name [120 bytes] (null terminated)
//!     Entry Data Size [u32 | 4 bytes]
//!     Entry Data Offset [u32 | 4 bytes]
//!
//! File Data [u8 | unknown size]
//! ```
//!
//! ## Version 1.1
//!
//! This version allows for a dynamically sized entry name, along with an increased character count allowing for 65535 characters, this allows for much longer entry name without using a lot of space.
//!
//! Flags were added to both the header and entries, these by default are zeroed out as they are custom and will change depending on which program is reading the file and how it will be used.
//!
//! ```lua
//! -- Header
//! Version [u32 | 4 bytes] (always 'LA11')
//! Flags [u32 | 4 bytes] (custom, used by a program)
//! Entry List Size [u32 | 4 bytes]
//!
//! -- File Entries
//!   -- For Each Entry
//!     Name Length [u16 | 2 bytes]
//!     Name Data [u8 | length determined by previous 2 bytes]
//!
//!     Flags [u32 | 4 bytes] (custom, used by a program)
//!
//!     Size [u32 | 4 bytes]
//!     Offset [u32 | 4 bytes]
//!
//! File Data [u8 | unknown size]
//! ```
//!
//! # Usage
//!
//! The crate is separated into a reader and writer (a reader/writer is planned, just not sure how to go about it).
//! This is because there normally isn't a need to modify an archive by a program or game after it was created.
//!
//! ### Writer
//!
//! This is a version basic example of using the archive writer, though it can't do much more, archives normally have more data than this.
//!
//! ```rust,no_run
//! use lar::{ArchiveWriter, WriteError, ArchiveVersion};
//!
//! fn main() -> Result<(), WriteError> {
//!     let mut writer = ArchiveWriter::default()?;
//!
//!     writer.add_data(
//!         // The entry name, normally a filepath
//!         "file.txt",
//!          // The entry contents, in this case a string.
//!          // If the data is large you can instead use `add_file`,
//!          // which will read a file only when being written
//!         "example file contents",
//!         // The entry flags (C bit field)
//!         None,
//!     );
//!
//!     // ArchiveVersion::default() will always return the newest format
//!     writer.write_file(ArchiveVersion::default(), "example.lar")?;
//!
//!     Ok(())
//! }
//! ```
//!
//! ## Reader
//!
//! You can read the file that was created above like so.
//!
//! ```rust,no_run
//! use lar::{ArchiveReader, ReadError};
//!
//! fn main() -> Result<(), ReadError> {
//!     let mut reader = ArchiveReader::read_file("example.lar")?;
//!
//!     println!("Archive Version: {}", reader.version());
//!
//!     println!("Archive Entries: {:?}", reader.entries());
//!
//!     if let Some(text) = reader.get_string("file.txt")? {
//!         println!("'file.txt' contents: {}", text);
//!     }
//!
//!     Ok(())
//! }
//! ```

mod de;
mod ser;
mod util;

pub use self::{
    de::{ArchiveReader, ReadError},
    ser::{ArchiveWriter, WriteError},
};

use std::fmt;

/// Compatible archive format versions that can be read from and written to.
#[derive(Clone, Copy, Debug, Hash, Ord, PartialOrd, Eq, PartialEq)]
pub enum ArchiveVersion {
    /// # Version 1.0
    ///
    /// Since version `1.0` was based off Quake's `.pak` format, it only changes and removed a few aspects.
    /// There is no entry table offset as the list is now right after the header (always 8 bytes from the beginning).
    /// And the 56 byte limit for the file name was upped to 120 bytes to allow for more complex file structures.
    ///
    /// ```lua
    /// -- Header
    /// Version [u32 | 4 bytes] (always 'LA10')
    /// Entry List Size [u32 | 4 bytes]
    ///
    /// -- Entry Header List [128 bytes] (repeats {Entry List Size / 128} times)
    ///   -- For Each Entry
    ///     Entry Name [120 bytes] (null terminated)
    ///     Entry Data Size [u32 | 4 bytes]
    ///     Entry Data Offset [u32 | 4 bytes]
    ///
    /// File Data [u8 | unknown size]
    /// ```
    OneZero,

    /// # Version 1.1
    ///
    /// This version allows for a dynamically sized entry name, along with an increased character count allowing for 65535 characters, this allows for much longer entry name without using a lot of space.
    ///
    /// Flags were added to both the header and entries, these by default are zeroed out as they are custom and will change depending on which program is reading the file and how it will be used.
    ///
    /// ```lua
    /// -- Header
    /// Version [u32 | 4 bytes] (always 'LA11')
    /// Flags [u32 | 4 bytes] (custom, used by a program)
    /// Entry List Size [u32 | 4 bytes]
    ///
    /// -- File Entries
    ///   -- For Each Entry
    ///     Name Length [u16 | 2 bytes]
    ///     Name Data [u8 | length determined by previous 2 bytes]
    ///
    ///     Flags [u32 | 4 bytes] (custom, used by a program)
    ///
    ///     Size [u32 | 4 bytes]
    ///     Offset [u32 | 4 bytes]
    ///
    /// File Data [u8 | unknown size]
    /// ```
    OneOne,
}

impl Default for ArchiveVersion {
    fn default() -> Self {
        ArchiveVersion::OneOne
    }
}

impl fmt::Display for ArchiveVersion {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ArchiveVersion::OneZero => write!(fmt, "v1.0"),
            ArchiveVersion::OneOne => write!(fmt, "v1.1"),
        }
    }
}
