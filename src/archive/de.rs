use {
    super::ArchiveVersion,
    std::{
        collections::HashMap,
        error::Error,
        fmt,
        fs::File,
        io::{self, BufReader, Cursor, Read, Seek, SeekFrom},
        path::Path,
        string,
    },
};

macro_rules! read {
    (u16; $buff:expr) => {{
        let mut buff_temp: [u8; 2] = [0; 2];
        $buff.read_exact(&mut buff_temp)?;
        u16::from_le_bytes(buff_temp)
    }};
    (u32; $buff:expr) => {{
        let mut buff_temp: [u8; 4] = [0; 4];
        $buff.read_exact(&mut buff_temp)?;
        u32::from_le_bytes(buff_temp)
    }};
}

#[derive(Debug)]
pub enum ReadError {
    UnsupportedVersion { version: (u8, u8, u8, u8) },

    Io { err: io::Error },
    Utf8 { err: string::FromUtf8Error },
}

impl From<io::Error> for ReadError {
    fn from(err: io::Error) -> Self {
        ReadError::Io { err }
    }
}

impl From<string::FromUtf8Error> for ReadError {
    fn from(err: string::FromUtf8Error) -> Self {
        ReadError::Utf8 { err }
    }
}

impl fmt::Display for ReadError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ReadError::UnsupportedVersion {
                version: (a, b, c, d),
            } => write!(fmt, "Unsupported Version: {}{}{}{}", a, b, c, d),
            ReadError::Io { err } => write!(fmt, "Io Error: {}", err),
            ReadError::Utf8 { err } => write!(fmt, "UTF-8 Conversion Error: {}", err),
        }
    }
}

impl Error for ReadError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            ReadError::Io { err } => Some(err),
            _ => None,
        }
    }
}

#[derive(Debug)]
struct Entry {
    flags: Option<u32>,
    size: u32,
    offset: u32,
}

pub struct ArchiveReader<R>
where
    R: Read + Seek,
{
    inner: ReaderInner<R>,
}

impl ArchiveReader<File> {
    pub fn read_file(path: impl AsRef<Path>) -> Result<Self, ReadError> {
        let file = File::open(path)?;

        let reader = BufReader::new(file);

        Self::read(reader)
    }
}

impl<R> ArchiveReader<R>
where
    R: Read + Seek,
{
    pub fn read(mut reader: BufReader<R>) -> Result<Self, ReadError> {
        // Read pak magic/version
        let mut buff_version: [u8; 4] = [0; 4];
        reader.read_exact(&mut buff_version)?;

        // Reset cursor to the beginning of the file
        reader.seek(SeekFrom::Start(0))?;

        match &buff_version {
            b"LA10" => {
                let reader = VersionOneZeroReader::read(reader)?;

                Ok(Self {
                    inner: ReaderInner::VersionOneZero { reader },
                })
            }
            b"LA11" => {
                let reader = VersionOneOneReader::read(reader)?;

                Ok(Self {
                    inner: ReaderInner::VersionOneOne { reader },
                })
            }
            [a, b, c, d] => Err(ReadError::UnsupportedVersion {
                version: (*a, *b, *c, *d),
            }),
        }
    }

    pub fn flags(&self) -> Option<u32> {
        match &self.inner {
            ReaderInner::VersionOneZero { reader } => reader.flags(),
            ReaderInner::VersionOneOne { reader } => reader.flags(),
        }
    }

    pub fn version(&self) -> ArchiveVersion {
        match &self.inner {
            ReaderInner::VersionOneZero { .. } => ArchiveVersion::OneZero,
            ReaderInner::VersionOneOne { .. } => ArchiveVersion::OneOne,
        }
    }

    pub fn entries(&self) -> Vec<&String> {
        match &self.inner {
            ReaderInner::VersionOneZero { reader } => reader.entries(),
            ReaderInner::VersionOneOne { reader } => reader.entries(),
        }
    }

    pub fn entry_flags(&self, key: impl Into<String>) -> Option<u32> {
        match &self.inner {
            ReaderInner::VersionOneZero { reader } => reader.entry_flags(key),
            ReaderInner::VersionOneOne { reader } => reader.entry_flags(key),
        }
    }

    pub fn get_bytes(&mut self, key: impl Into<String>) -> Result<Option<Vec<u8>>, ReadError> {
        match &mut self.inner {
            ReaderInner::VersionOneZero { reader } => reader.get_bytes(key),
            ReaderInner::VersionOneOne { reader } => reader.get_bytes(key),
        }
    }

    pub fn get_string(&mut self, key: impl Into<String>) -> Result<Option<String>, ReadError> {
        match self.get_bytes(key)? {
            Some(bytes) => {
                let text = String::from_utf8(bytes)?;

                Ok(Some(text))
            }
            None => Ok(None),
        }
    }
}

enum ReaderInner<R>
where
    R: Read + Seek,
{
    VersionOneZero { reader: VersionOneZeroReader<R> },
    VersionOneOne { reader: VersionOneOneReader<R> },
}

struct VersionOneZeroReader<R>
where
    R: Read + Seek,
{
    reader: BufReader<R>,
    entries: HashMap<String, Entry>,
}

impl<R> VersionOneZeroReader<R>
where
    R: Read + Seek,
{
    fn read(mut reader: BufReader<R>) -> Result<Self, ReadError> {
        // Move past version and to size
        reader.seek(SeekFrom::Start(4))?;

        // Get entry header list size
        let header_size = read!(u32; reader);

        // A entry header is 128 bytes, divide the size by 128 to get the count
        let header_count = header_size / 128;

        // Move to the beginning of the entry header list
        reader.seek(SeekFrom::Start(8))?;

        let mut entries = HashMap::new();

        // Read entry headers into memory
        for _ in 0..header_count {
            // Read the name of the entry
            let mut buff_entry_name: [u8; 120] = [0; 120];
            reader.read_exact(&mut buff_entry_name)?;

            // Trim the null terminator
            let null_terminator = buff_entry_name
                .iter()
                .position(|&byte| byte == b'\0')
                .expect("Entry name has no null terminator, malformed archive?");
            let entry_name_raw = &buff_entry_name[..null_terminator];

            // Convert the raw array of bytes to UTF-8 string
            let entry_name = String::from_utf8(entry_name_raw.to_vec())?;

            // Read the data offset of the entry
            let entry_offset = read!(u32; reader);

            // Read the size of the entry
            let entry_size = read!(u32; reader);

            // Add entry to the map
            entries.insert(
                entry_name,
                Entry {
                    flags: None,
                    size: entry_size,
                    offset: entry_offset,
                },
            );
        }

        Ok(Self { reader, entries })
    }

    pub fn flags(&self) -> Option<u32> {
        None
    }

    pub fn entries(&self) -> Vec<&String> {
        self.entries.keys().collect()
    }

    pub fn entry_flags(&self, _: impl Into<String>) -> Option<u32> {
        None
    }

    fn get_bytes(&mut self, key: impl Into<String>) -> Result<Option<Vec<u8>>, ReadError> {
        match self.entries.get(&key.into()) {
            Some(Entry { size, offset, .. }) => {
                self.reader.seek(SeekFrom::Start((*offset) as u64))?;

                let mut buff = Vec::with_capacity((*size) as usize);

                {
                    let reader_ref = Read::by_ref(&mut self.reader);

                    reader_ref.take((*size) as u64).read_to_end(&mut buff)?;
                }

                Ok(Some(buff))
            }
            None => Ok(None),
        }
    }
}

struct VersionOneOneReader<R>
where
    R: Read + Seek,
{
    reader: BufReader<R>,
    flags: u32,
    entries: HashMap<String, Entry>,
}

impl<R> VersionOneOneReader<R>
where
    R: Read + Seek,
{
    fn read(mut reader: BufReader<R>) -> Result<Self, ReadError> {
        // Move past version and to flags
        reader.seek(SeekFrom::Start(4))?;

        // Get header flags
        let flags = read!(u32; reader);

        // Get entry header list size
        let header_size = read!(u32; reader);

        let mut buff_raw_entry_list = Vec::with_capacity(header_size as usize);

        {
            let reader_ref = Read::by_ref(&mut reader);

            reader_ref
                .take(header_size as u64)
                .read_to_end(&mut buff_raw_entry_list)?;
        }

        let mut entries = HashMap::new();

        let entry_list_length = buff_raw_entry_list.len();
        let mut cursor_raw_entry_list = Cursor::new(buff_raw_entry_list);

        loop {
            // Read entry name length
            let name_length = read!(u16; cursor_raw_entry_list);

            // Read the entry name baed off size bytes
            let mut buff_raw_name = Vec::with_capacity(name_length as usize);

            {
                let vec_ref = Read::by_ref(&mut cursor_raw_entry_list);

                vec_ref
                    .take(name_length as u64)
                    .read_to_end(&mut buff_raw_name)?;
            }

            // Attempt to convert byte vector to string
            let entry_name = String::from_utf8(buff_raw_name)?;

            // Get entry flags
            let entry_flags = read!(u32; cursor_raw_entry_list);

            // Read the size of the entry
            let entry_size = read!(u32; cursor_raw_entry_list);

            // Read the data offset of the entry
            let entry_offset = read!(u32; cursor_raw_entry_list);

            // Add entry to the map
            entries.insert(
                entry_name,
                Entry {
                    flags: Some(entry_flags),
                    size: entry_size,
                    offset: entry_offset,
                },
            );

            // Stop loop if seek is at the end of the stream
            if cursor_raw_entry_list.position() == entry_list_length as u64 {
                break;
            }
        }

        Ok(Self {
            reader,
            flags,
            entries,
        })
    }

    pub fn flags(&self) -> Option<u32> {
        Some(self.flags)
    }

    pub fn entries(&self) -> Vec<&String> {
        self.entries.keys().collect()
    }

    pub fn entry_flags(&self, key: impl Into<String>) -> Option<u32> {
        self.entries.get(&key.into()).and_then(|e| e.flags)
    }

    fn get_bytes(&mut self, key: impl Into<String>) -> Result<Option<Vec<u8>>, ReadError> {
        match self.entries.get(&key.into()) {
            Some(Entry { size, offset, .. }) => {
                self.reader.seek(SeekFrom::Start((*offset) as u64))?;

                let mut buff = Vec::with_capacity((*size) as usize);

                {
                    let reader_ref = Read::by_ref(&mut self.reader);

                    reader_ref.take((*size) as u64).read_to_end(&mut buff)?;
                }

                Ok(Some(buff))
            }
            None => Ok(None),
        }
    }
}
