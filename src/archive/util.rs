use core::cmp::Ordering;

// From https://github.com/magiclen/alphanumeric-sort
// This was copied to make changes late
#[allow(clippy::while_let_on_iterator)]
pub fn compare(a: impl AsRef<str>, b: impl AsRef<str>) -> Ordering {
    let mut a_chars = a.as_ref().chars();
    let mut b_chars = b.as_ref().chars();

    let mut a_buff = None;
    let mut b_buff = None;

    loop {
        // Get next character in input A
        let a_char = match a_buff.take() {
            Some(c) => c,
            None => {
                match a_chars.next() {
                    Some(c) => c,
                    None => {
                        if b_buff.take().is_some() || b_chars.next().is_some() {
                            // Input A is sorter than input B
                            return Ordering::Less;
                        } else {
                            return Ordering::Equal;
                        }
                    }
                }
            }
        };

        // Get next character in input B
        let b_char = match b_buff.take() {
            Some(c) => c,
            None => match b_chars.next() {
                Some(c) => c,
                None => {
                    return Ordering::Greater;
                }
            },
        };

        if a_char >= '0' && a_char <= '9' && b_char >= '0' && b_char <= '9' {
            // If character is a number
            let mut a_num = f64::from(a_char as u32) - f64::from(b'0');
            let mut b_num = f64::from(b_char as u32) - f64::from(b'0');

            while let Some(a_char) = a_chars.next() {
                if a_char >= '0' && a_char <= '9' {
                    a_num = a_num * 10.0 + (f64::from(a_char as u32) - f64::from(b'0'));
                } else {
                    a_buff = Some(a_char);
                    break;
                }
            }

            while let Some(b_char) = b_chars.next() {
                if b_char >= '0' && b_char <= '9' {
                    b_num = b_num * 10.0 + (f64::from(b_char as u32) - f64::from(b'0'));
                } else {
                    b_buff = Some(b_char);
                    break;
                }
            }

            match a_num.partial_cmp(&b_num) {
                Some(ordering) if ordering != Ordering::Equal => {
                    return ordering;
                }
                _ => (),
            }
        } else {
            // Character is not a number, compare like normal
            match a_char.cmp(&b_char) {
                Ordering::Equal => (),
                Ordering::Greater => {
                    return if (a_char > (255 as char)) ^ (b_char > (255 as char)) {
                        Ordering::Less
                    } else {
                        Ordering::Greater
                    };
                }
                Ordering::Less => {
                    return if (a_char > (255 as char)) ^ (b_char > (255 as char)) {
                        Ordering::Greater
                    } else {
                        Ordering::Less
                    };
                }
            }
        }
    }
}
