use crate::{lit::Lit, span::Span};

pub trait EmptyToken {
    fn into_empty() -> Token;
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Ident {
    pub value: String,
    pub span: Span,
}

impl Ident {
    pub fn new(value: String, span: impl Into<Span>) -> Self {
        Self {
            value,
            span: span.into(),
        }
    }
}

impl From<Ident> for Token {
    fn from(inner: Ident) -> Token {
        Token::Ident(inner)
    }
}

macro_rules! define_whitespace {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    }
}

define_whitespace! {
    EOF             /// `end of file`
    NewLine         /// `\n`/`\r\n`
    SOF             /// `start of file`
    Space           /// ` `
    Tab             /// `\t`
}

macro_rules! define_keywords {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_keywords! {
    As              /// `as`
    Break           /// `break`
    Const           /// `const`
    Continue        /// `continue`
    Else            /// `else`
    Enum            /// `enum`
    For             /// `for`
    Function        /// `fn`
    If              /// `if`
    Impl            /// `impl`
    In              /// `in`
    Infix           /// `infix`
    Let             /// `let`
    Loop            /// `loop`
    Match           /// `match`
    Mod             /// `mod`
    Mut             /// `mut`
    Pub             /// `pub`
    Return          /// `return`
    SelfType        /// `Self`
    SelfValue       /// `self`
    Struct          /// `struct`
    Trait           /// `trait`
    TypeAlias       /// `typealias`
    Use             /// `use`
    VarArg          /// `vararg`
    Where           /// `where`
    While           /// `while`
}

macro_rules! define_symbols {
    ( $( $name:ident #[$doc:meta] )* ) => {
        $(
            #[$doc]
            #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
            pub struct $name {
                pub span: Span,
            }

            impl $name {
                pub fn new(span: impl Into<Span>) -> Self {
                    Self {
                        span: span.into(),
                    }
                }
            }

            impl std::fmt::Display for $name {
                fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                    write!(f, "{}{}", stringify!($name), self.span)
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_symbols! {
    Add             /// `+`
    AddEq           /// `+=`
    And             /// `&`
    AndAnd          /// `&&`
    AndEq           /// `&=`
    At              /// `@`
    Bang            /// `!`
    BraceClose      /// `}`
    BraceOpen       /// `{`
    BracketClose    /// `]`
    BracketOpen     /// `[`
    Caret           /// `^`
    CaretEq         /// `^=`
    Colon           /// `:`
    ColonColon      /// `::`
    Comma           /// `,`
    Div             /// `/`
    DivEq           /// `/=`
    Dot             /// `.`
    DotDot          /// `..`
    DotDotEq        /// `..=`
    Eq              /// `=`
    EqEq            /// `==`
    Ge              /// `>=`
    Gt              /// `>`
    LArrow          /// `<-`
    Le              /// `<=`
    Lt              /// `<`
    Mul             /// `*`
    MulEq           /// `*=`
    Ne              /// `!=`
    Or              /// `|`
    OrOr            /// `||`
    ParenClose      /// `)`
    ParenOpen       /// `(`
    Pound           /// `#`
    Question        /// `?`
    RArrow          /// `->`
    Rem             /// `%`
    RemEq           /// `%=`
    Shl             /// `<<`
    ShlEq           /// `<<=`
    Shr             /// `>>`
    ShrEq           /// `>>=`
    Sub             /// `-`
    SubEq           /// `-=`
    Tilde           /// `~`
    Underscore      /// `_`
}

macro_rules! define_token {
    ( $( $name:ident #[$doc:meta] )* ) => {
        #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
        pub enum Token {
            Ident(Ident),
            Literal(Lit),
            $(
                #[$doc]
                $name($name),
            )*
        }

        impl Token {
            pub fn new(inner: impl Into<Token>) -> Token {
                inner.into()
            }
        }

        $(
            impl From<$name> for Token {
                fn from(inner: $name) -> Token {
                    Token::$name(inner)
                }
            }

            impl EmptyToken for $name {
                fn into_empty() -> Token {
                    Token::$name($name::new(
                        Span::default()
                    ))
                }
            }
        )*
    };
}

#[rustfmt::skip]
define_token! {
    EOF             /// `end of file`
    NewLine         /// `\n`/`\r\n`
    SOF             /// `start of file`
    Space           /// ` `
    Tab             /// `\t`

    As              /// `as`
    Break           /// `break`
    Const           /// `const`
    Continue        /// `continue`
    Else            /// `else`
    Enum            /// `enum`
    For             /// `for`
    Function        /// `fn`
    If              /// `if`
    Impl            /// `impl`
    In              /// `in`
    Infix           /// `infix`
    Let             /// `let`
    Loop            /// `loop`
    Match           /// `match`
    Mod             /// `mod`
    Mut             /// `mut`
    Pub             /// `pub`
    Return          /// `return`
    SelfType        /// `Self`
    SelfValue       /// `self`
    Struct          /// `struct`
    Trait           /// `trait`
    TypeAlias       /// `typealias`
    Use             /// `use`
    VarArg          /// `vararg`
    Where           /// `where`
    While           /// `while`

    Add             /// `+`
    AddEq           /// `+=`
    And             /// `&`
    AndAnd          /// `&&`
    AndEq           /// `&=`
    At              /// `@`
    Bang            /// `!`
    BraceClose      /// `}`
    BraceOpen       /// `{`
    BracketClose    /// `]`
    BracketOpen     /// `[`
    Caret           /// `^`
    CaretEq         /// `^=`
    Colon           /// `:`
    ColonColon      /// `::`
    Comma           /// `,`
    Div             /// `/`
    DivEq           /// `/=`
    Dot             /// `.`
    DotDot          /// `..`
    DotDotEq        /// `..=`
    Eq              /// `=`
    EqEq            /// `==`
    Ge              /// `>=`
    Gt              /// `>`
    LArrow          /// `<-`
    Le              /// `<=`
    Lt              /// `<`
    Mul             /// `*`
    MulEq           /// `*=`
    Ne              /// `!=`
    Or              /// `|`
    OrOr            /// `||`
    ParenClose      /// `)`
    ParenOpen       /// `(`
    Pound           /// `#`
    Question        /// `?`
    RArrow          /// `->`
    Rem             /// `%`
    RemEq           /// `%=`
    Shl             /// `<<`
    ShlEq           /// `<<=`
    Shr             /// `>>`
    ShrEq           /// `>>=`
    Sub             /// `-`
    SubEq           /// `-=`
    Tilde           /// `~`
    Underscore      /// `_`
}

#[rustfmt::skip]
#[macro_export]
macro_rules! T {
    (as)            => { $crate::token::As };
    (break)         => { $crate::token::Break };
    (const)         => { $crate::token::Const };
    (continue)      => { $crate::token::Continue };
    (else)          => { $crate::token::Else };
    (enum)          => { $crate::token::Enum };
    (fn)            => { $crate::token::Function };
    (for)           => { $crate::token::For };
    (if)            => { $crate::token::If };
    (impl)          => { $crate::token::Impl };
    (in)            => { $crate::token::In };
    (infix)         => { $crate::token::Index };
    (let)           => { $crate::token::Let };
    (loop)          => { $crate::token::Loop };
    (match)         => { $crate::token::Match };
    (mod)           => { $crate::token::Mod };
    (mut)           => { $crate::token::Mut };
    (pub)           => { $crate::token::Pub };
    (return)        => { $crate::token::Return };
    (self)          => { $crate::token::SelfType };
    (Self)          => { $crate::token::SelfVar };
    (struct)        => { $crate::token::Struct };
    (trait)         => { $crate::token::Trait };
    (typealias)     => { $crate::token::TypeAlias };
    (use)           => { $crate::token::Use };
    (vararg)        => { $crate::token::VarArg };
    (where)         => { $crate::token::Where };
    (while)         => { $crate::token::While };

    (+)             => { $crate::token::Add };
    (+=)            => { $crate::token::AddEq };
    (&)             => { $crate::token::And };
    (&&)            => { $crate::token::AndAnd };
    (&=)            => { $crate::token::AndEq };
    (@)             => { $crate::token::At };
    (!)             => { $crate::token::Bang };
    (^)             => { $crate::token::Caret };
    (^=)            => { $crate::token::CaretEq };
    (:)             => { $crate::token::Colon };
    (::)            => { $crate::token::ColonColon };
    (,)             => { $crate::token::Comma };
    (/)             => { $crate::token::Div };
    (/=)            => { $crate::token::DivEq };
    (.)             => { $crate::token::Dot };
    (..)            => { $crate::token::DotDot };
    (..=)           => { $crate::token::DotDotEq };
    (=)             => { $crate::token::Eq };
    (==)            => { $crate::token::EqEq };
    (>=)            => { $crate::token::Ge };
    (>)             => { $crate::token::Gt };
    (<-)            => { $crate::token::LArrow };
    (<=)            => { $crate::token::Le };
    (<)             => { $crate::token::Lt };
    (*)             => { $crate::token::Mul };
    (*=)            => { $crate::token::MulEq };
    (!=)            => { $crate::token::Ne };
    (|)             => { $crate::token::Or };
    (||)            => { $crate::token::OrOr };
    (#)             => { $crate::token::Pound };
    (?)             => { $crate::token::Question };
    (->)            => { $crate::token::RArrow };
    (%)             => { $crate::token::Rem };
    (%=)            => { $crate::token::RemEq };
    (<<)            => { $crate::token::Shl };
    (<<=)           => { $crate::token::ShlEq };
    (>>)            => { $crate::token::Shr };
    (>>=)           => { $crate::token::ShrEq };
    (-)             => { $crate::token::Sub };
    (-=)            => { $crate::token::SubEq };
    (~)             => { $crate::token::Tilde };
    (_)             => { $crate::token::Underscore };
}
