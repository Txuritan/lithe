use crate::{punctuated::Punctuated, token::Ident};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Path<'t> {
    pub segments: Punctuated<PathSegment<'t>, bool>,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct PathSegment<'t> {
    pub ident: &'t Ident,
    // pub arguments: PathArguments,
}

// #[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
// pub enum PathArguments {
//     None,
//     AngleBracketed(AngleBracketedGenericArguments),
//     Parenthesized(ParenthesizedGenericArguments),
// }
