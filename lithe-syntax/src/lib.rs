#[macro_use]
pub mod token;

pub mod stream;

pub mod lexer;
pub mod parser;

pub mod expr;
pub mod ident;
pub mod item;
pub mod lit;
pub mod op;
pub mod pat;
pub mod path;
pub mod punctuated;
pub mod span;
pub mod stmt;
pub mod typ;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Error {
    UnexpectedEOF,
    UnexpectedPunctuatedState(bool, bool),
    UnexpectedToken(String),
}
