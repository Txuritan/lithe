use crate::{
    stream,
    token::{Colon, Ident, Mut, Token},
    typ::Type,
    Error,
};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub enum Pat<'t> {
    Ident(PatIdent<'t>),
    Path(),
    Range(),
    Rest(),
    Slice(),
    Struct(),
    Tuple(),
    TupleStruct(),
    Type(PatType<'t>),
    Wild(),
}

impl<'t> stream::Parse<'t> for Pat<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        match ctx.step(|mut ctx| {
            let ast = PatIdent::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => return Ok(Pat::Ident(ast)),
            Err(Error::UnexpectedToken(_)) => {}
            Err(err) => return Err(err),
        }

        match ctx.step(|mut ctx| {
            let ast = PatType::parse(&mut ctx)?;

            Ok((ast, ctx))
        }) {
            Ok(ast) => Ok(Pat::Type(ast)),
            Err(err) => Err(err),
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct PatIdent<'t> {
    // TODO: change to attribute type
    pub attrs: Vec<String>,
    pub mutability: Option<&'t Mut>,
    pub ident: &'t Ident,
}

impl<'t> stream::Parse<'t> for PatIdent<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let (mutability, ident) = match ctx.next_skip() {
            Some(Token::Mut(keyword_mut)) => match ctx.next_skip() {
                Some(Token::Ident(ident)) => (Some(keyword_mut), ident),
                Some(token) => {
                    return Err(Error::UnexpectedToken(format!("{:?}", token)));
                }
                None => {
                    return Err(Error::UnexpectedEOF);
                }
            },
            Some(Token::Ident(ident)) => (None, ident),
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        Ok(Self {
            attrs: Vec::new(),
            mutability,
            ident,
        })
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct PatType<'t> {
    // TODO: change to attribute type
    pub attrs: Vec<String>,
    pub pat: Box<Pat<'t>>,
    pub colon: &'t Colon,
    pub typ: Box<Type<'t>>,
}

impl<'t> stream::Parse<'t> for PatType<'t> {
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let ident = PatIdent::parse(ctx)?;

        let colon = match ctx.next_skip() {
            Some(Token::Colon(colon)) => colon,
            Some(token) => {
                return Err(Error::UnexpectedToken(format!("{:?}", token)));
            }
            None => {
                return Err(Error::UnexpectedEOF);
            }
        };

        let typ = Type::parse(ctx)?;

        Ok(Self {
            attrs: Vec::new(),
            pat: Box::new(Pat::Ident(ident)),
            colon,
            typ: Box::new(typ),
        })
    }
}
