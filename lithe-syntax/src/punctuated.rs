use {
    crate::{stream, Error},
    std::fmt,
};

// Taken straight from syn
#[derive(Clone, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Punctuated<T, P> {
    pub inner: Vec<(T, P)>,
    pub last: Option<T>,
}

impl<T, P> Punctuated<T, P> {
    pub fn new() -> Self {
        Self::default()
    }
}

impl<T, P> Default for Punctuated<T, P> {
    fn default() -> Self {
        Self {
            inner: Vec::new(),
            last: None,
        }
    }
}

impl<T: fmt::Debug, P: fmt::Debug> fmt::Debug for Punctuated<T, P> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut list = f.debug_list();

        for (t, p) in &self.inner {
            list.entry(t);
            list.entry(p);
        }

        if let Some(last) = &self.last {
            list.entry(last);
        }

        list.finish()
    }
}

impl<'t, T, P> stream::Parse<'t> for Punctuated<T, P>
where
    T: stream::Parse<'t>,
    P: stream::Parse<'t>,
{
    fn parse(ctx: &mut stream::Stream<'t>) -> Result<Self, Error> {
        let mut pun = Self::new();

        let mut value: Option<T> = None;
        let mut punct: Option<P> = None;

        loop {
            match ctx.step(|mut ctx| {
                let ast = T::parse(&mut ctx)?;

                Ok((ast, ctx))
            }) {
                Ok(ast) => value = Some(ast),
                Err(Error::UnexpectedToken(_)) => break,
                Err(err) => {
                    return Err(err);
                }
            }

            match ctx.step(|mut ctx| {
                let ast = P::parse(&mut ctx)?;

                Ok((ast, ctx))
            }) {
                Ok(ast) => punct = Some(ast),
                Err(Error::UnexpectedToken(_)) => break,
                Err(err) => {
                    return Err(err);
                }
            }

            match (value.take(), punct.take()) {
                (Some(value), Some(punct)) => pun.inner.push((value, punct)),
                (value, punct) => {
                    return Err(Error::UnexpectedPunctuatedState(
                        value.is_some(),
                        punct.is_some(),
                    ));
                }
            }
        }

        match (value.take(), punct.take()) {
            (Some(value), None) => pun.last = Some(value),
            (None, None) => {}
            (value, punct) => {
                return Err(Error::UnexpectedPunctuatedState(
                    value.is_some(),
                    punct.is_some(),
                ));
            }
        }

        Ok(pun)
    }
}
