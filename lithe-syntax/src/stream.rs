use {
    crate::{token::{Token, EmptyToken}, Error},
    std::{iter::Iterator, mem::discriminant},
};

pub trait Peek {
    fn peek(token: &Token) -> bool;
}

pub trait Parse<'t>: Sized {
    fn parse(ctx: &mut Stream<'t>) -> Result<Self, Error>;
}

pub struct Stream<'t> {
    tokens: &'t [Token],

    index: usize,
}

impl<'t> Stream<'t> {
    pub fn new(tokens: &'t [Token]) -> Stream<'t> {
        Stream { tokens, index: 0 }
    }

    pub fn parse<P>(&mut self) -> Result<P, Error>
    where
        P: Parse<'t>,
    {
        P::parse(self)
    }

    pub fn call<P>(&mut self, fun: fn(&mut Self) -> Result<P, Error>) -> Result<P, Error> {
        fun(self)
    }

    pub fn current(&self) -> Option<&'t Token> {
        self.tokens.get(self.index)
    }

    pub fn peek_amount(&self, amount: usize) -> Option<&'t Token> {
        self.tokens.get(self.index + amount)
    }

    pub fn peek(&self) -> Option<&'t Token> {
        self.peek_amount(1)
    }

    pub fn is_amount<I>(&self, amount: usize) -> bool
    where
        I: Peek,
    {
        self.peek_amount(amount)
            .map(I::peek)
            .unwrap_or_else(|| false)
    }

    pub fn is<I>(&self) -> bool
    where
        I: Peek,
    {
        self.is_amount::<I>(1)
    }

    pub fn fork(&self) -> Stream<'t> {
        Stream {
            tokens: self.tokens,

            index: self.index,
        }
    }

    pub fn step<P>(
        &mut self,
        fun: impl FnOnce(Self) -> Result<(P, Self), Error>,
    ) -> Result<P, Error> {
        let (node, stream) = fun(self.fork())?;

        self.index = stream.index;

        Ok(node)
    }

    pub fn next_skip(&mut self) -> Option<&'t Token> {
        loop {
            self.index += 1;

            match self.tokens.get(self.index) {
                Some(Token::Space(_)) | Some(Token::Tab(_)) => {
                    continue;
                }
                Some(Token::NewLine(_)) => {
                    continue;
                }
                Some(Token::SOF(_)) | Some(Token::EOF(_)) | None => {
                    return None;
                }
                token => return token,
            }
        }
    }

    pub fn peek_skip(&self) -> Option<&'t Token> {
        let mut amount = 0;

        loop {
            amount += 1;

            match self.peek_amount(amount) {
                Some(Token::Space(_)) | Some(Token::Tab(_)) => {
                    continue;
                }
                Some(Token::NewLine(_)) => {
                    continue;
                }
                Some(Token::SOF(_)) | Some(Token::EOF(_)) | None => {
                    return None;
                }
                token => return token,
            }
        }
    }

    pub fn assert_token<T>(&mut self) -> Result<&Token, Error>
    where
        T: EmptyToken,
    {
        match self.next_skip() {
            Some(token) => {
                if discriminant(token) == discriminant(&T::into_empty()) {
                    Ok(token)
                } else {
                    Err(Error::UnexpectedToken(format!("{:?}", token)))
                }
            }
            None => {
                Err(Error::UnexpectedEOF)
            }
        }
    }
}

impl<'t> Iterator for Stream<'t> {
    type Item = &'t Token;

    fn next(&mut self) -> Option<Self::Item> {
        self.index += 1;

        self.current()
    }
}
