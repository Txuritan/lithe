use {
    super::{util, ArchiveVersion},
    std::{
        error::Error,
        fmt,
        fs::File,
        io::{self, BufWriter, Write},
        mem::replace,
        path::{Path, PathBuf},
    },
};

#[derive(Debug)]
pub enum WriteError {
    EntryNameLength,

    Io { err: io::Error },
}

impl From<io::Error> for WriteError {
    fn from(err: io::Error) -> Self {
        WriteError::Io { err }
    }
}

impl fmt::Display for WriteError {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            WriteError::EntryNameLength => write!(fmt, "An Entry Name Is Longer Than Allowed"),
            WriteError::Io { err } => write!(fmt, "Io Error: {}", err),
        }
    }
}

impl Error for WriteError {}

#[derive(Default)]
pub struct ArchiveWriter {
    flags: Option<u32>,
    files: Vec<Entry>,
}

impl ArchiveWriter {
    pub fn flags(&mut self, flags: u32) {
        self.flags = Some(flags);
    }

    pub fn add_data(
        &mut self,
        entry_name: impl Into<String>,
        data: impl Into<Vec<u8>>,
        flags: Option<u32>,
    ) {
        let data = data.into();

        self.files.push(Entry {
            name: entry_name.into(),
            size: data.len() as u32,
            data: Either::Data(data),
            flags,
        });

        self.files.sort_by(|a, b| util::compare(&a.name, &b.name));
    }

    pub fn add_file(
        &mut self,
        entry_name: impl Into<String>,
        file_path: impl Into<PathBuf>,
        flags: Option<u32>,
    ) -> Result<(), WriteError> {
        let file_path = file_path.into();

        let data = std::fs::read(&file_path)?;

        self.files.push(Entry {
            name: entry_name.into(),
            size: data.len() as u32,
            data: Either::Path(file_path),
            flags,
        });

        self.files.sort_by(|a, b| util::compare(&a.name, &b.name));

        Ok(())
    }

    pub fn write(self, version: ArchiveVersion, path: impl AsRef<Path>) -> Result<(), WriteError> {
        match version {
            ArchiveVersion::OneZero => VersionOneZeroWriter::write(self.files, path)?,
            ArchiveVersion::OneOne => VersionOneOneWriter::write(self.flags, self.files, path)?,
        }

        Ok(())
    }
}

struct Entry {
    name: String,
    data: Either,
    size: u32,
    flags: Option<u32>,
}

enum Either {
    Data(Vec<u8>),
    Path(std::path::PathBuf),
}

struct VersionOneZeroWriter {}

impl VersionOneZeroWriter {
    pub fn write(files: Vec<Entry>, path: impl AsRef<Path>) -> Result<(), WriteError> {
        let file = File::create(path)?;
        let mut writer = BufWriter::new(file);

        // magic
        writer.write_all(b"LA10")?;

        let header_size = (files.len() * 128) as u32;

        // entry header size
        writer.write_all(&(header_size.to_le_bytes()))?;

        // header size plus entry header list size
        // used for entry data offsets
        let mut data_offset = 8 + header_size;

        for file in &files {
            let mut entry_name = (&file.name).as_bytes().to_vec();

            // add null byte to entry name
            entry_name.push(b'\0');

            // The full buffered file name
            let mut name: [u8; 120] = [0; 120];

            // move name data into fixed array
            for (i, name_byte) in entry_name.into_iter().enumerate() {
                name[i] = name_byte;
            }

            // write entry name to the list
            writer.write_all(&name)?;

            // write offset
            writer.write_all(&data_offset.to_le_bytes())?;

            // write size
            writer.write_all(&(file.size).to_le_bytes())?;

            // Add entry size to offset
            data_offset += file.size;
        }

        for Entry { data: either, .. } in files {
            let data = match either {
                Either::Data(data) => data,
                Either::Path(path) => std::fs::read(path)?,
            };

            writer.write_all(&data)?;
        }

        Ok(())
    }
}

struct VersionOneOneWriter {}

impl VersionOneOneWriter {
    pub fn write(
        flags: Option<u32>,
        files: Vec<Entry>,
        path: impl AsRef<Path>,
    ) -> Result<(), WriteError> {
        let file = File::create(path)?;
        let mut writer = BufWriter::new(file);

        // magic
        writer.write_all(b"LA11")?;

        // write flags
        writer.write_all(&(flags.unwrap_or_else(|| 0)).to_le_bytes())?;

        // entry list to get size later
        // the lengths added up in the entry list size
        let mut entry_list = Vec::new();

        for file in &files {
            let mut entry_buffer = Vec::new();

            let entry_name = (&file.name).as_bytes();

            let name_len = entry_name.len() as u16;

            // write name length
            entry_buffer.write_all(&name_len.to_le_bytes())?;

            // write name
            entry_buffer.write_all(entry_name)?;

            // write flags
            entry_buffer.write_all(
                &(&file.flags)
                    .as_ref()
                    .copied()
                    .unwrap_or_else(|| 0)
                    .to_le_bytes(),
            )?;

            // write empty size
            entry_buffer.write_all(&(file.size).to_le_bytes())?;

            // will be written to in the second pass before entry data
            // write empty offset
            entry_buffer.write_all(&0u32.to_le_bytes())?;

            // shrink to make sure to use as little space as possible
            entry_buffer.shrink_to_fit();

            entry_list.push(entry_buffer);
        }

        // add up all the entries lengths
        let entry_list_size = {
            let mut size = 0u32;

            for entry in &entry_list {
                size += entry.len() as u32;
            }

            size
        };

        // write entry list size
        writer.write_all(&entry_list_size.to_le_bytes())?;

        // to track where file are stored in the archive
        // the 12 is the header size
        let mut data_offset = 12 + entry_list_size;

        for (entry, buffer) in files.iter().zip(entry_list.iter_mut()) {
            // convert current offset to bytes
            let [b1, b2, b3, b4] = data_offset.to_le_bytes();

            let len = buffer.len();

            // replace the old last 4 offset bytes with the new
            let _ = replace(&mut buffer[len - 4], b1);
            let _ = replace(&mut buffer[len - 3], b2);
            let _ = replace(&mut buffer[len - 2], b3);
            let _ = replace(&mut buffer[len - 1], b4);

            // increase the offset by the current entry data size
            data_offset += entry.size;
        }

        // write each entry to the entry list in the buffer
        for buff in entry_list.iter_mut() {
            writer.write_all(buff)?;
        }

        // clear any remaining data
        // TODO: check for remaining data
        entry_list.clear();

        // write the entry data to the end of the file
        for Entry { data: either, .. } in files {
            let data = match either {
                Either::Data(data) => data,
                Either::Path(path) => std::fs::read(path)?,
            };

            writer.write_all(&data)?;
        }

        Ok(())
    }
}
