# Lithe

Lithe is a a embeddable scripting language, designed for use with games.
It takes many features from Rust and Kotlin, along with others like Wren, MoonScript, and Python.

## Lithe Archive (LAR)

A LAR is Lithe's equivalent to Java's JAR file.
It's used to store a programs compiled code, runtime data, and other information needed for a modding system.

Instead of being based off ZIP, the LAR format however uses the [AR format](https://en.wikipedia.org/wiki/Ar_%28Unix%29), specifically the BSD variant.
